export class Account {
  email!: string;
  amount!: number;
  iban!: string;
  firstName!: string;
  lastName!: string;
}
