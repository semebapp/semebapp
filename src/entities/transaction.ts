
export class Transaction {
  id!: number;
  from!: string;
  to!: string;
  fromAmount!: number;
  toAmount!: number;
  transactionDate!: Date;
  description!: string;
}
