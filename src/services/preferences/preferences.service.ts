import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Preferences } from 'src/entities/preferences';

@Injectable({
  providedIn: 'root'
})
export class PreferencesService {

  constructor(private http: HttpClient) { }

  getPreferences(id: string): Observable<Preferences> {
    return this.http.get<Preferences>(`/api/preferences/${id}`);
  }

  createPreferences(preferences: Preferences): Observable<Preferences> {
    return this.http.post<Preferences>(`/api/preferences`, preferences);
  }

  deletePreferences(preferences: Preferences): Observable<Preferences> {
    return this.http.delete<Preferences>(`/api/preferences/${preferences.user}`);
  }

  updatePreferences(preferences: Preferences): Observable<Preferences> {
    return this.http.put<Preferences>(`/api/preferences/${preferences.user}`, preferences);
  }

}
