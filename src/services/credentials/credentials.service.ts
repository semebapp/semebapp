import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Account } from 'src/entities/account';

@Injectable({
  providedIn: 'root'
})
export class CredentialsService {

  public account$ = new BehaviorSubject<Account | null>(null);

  constructor(private router: Router) {
    let accountString = localStorage.getItem("account");
    if (accountString !== null) {
      var account = JSON.parse(accountString);
      this.account$ = new BehaviorSubject(account);
    }
  }

  setCredentials(account: Account) {
    localStorage.setItem("account", JSON.stringify(account));
    this.account$.next(account);
  }

  removeCredentials() {
    localStorage.removeItem("account");
    this.account$.next(null);
    this.router.navigateByUrl('login');
  }
}
