import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from 'src/entities/account';
import { Transaction } from 'src/entities/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) { }

  getTransactionsForAccount(account: Account): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`/api/transaction/${account.email}`);
  }

  createTransaction(transaction: Transaction): Observable<Transaction> {
    return this.http.post<Transaction>(`/api/transaction`, transaction);
  }
}
