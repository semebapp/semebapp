import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from 'src/entities/account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getAccount(id: string): Observable<Account> {
    return this.http.get<Account>(`/api/account/${id}`);
  }

  createAccount(account: Account): Observable<Account> {
    return this.http.post<Account>(`/api/account`, account);
  }

  deleteAccount(account: Account): Observable<Account> {
    return this.http.delete<Account>(`/api/account/${account.email}`);
  }

  updateAccount(account: Account): Observable<Account> {
    return this.http.put<Account>(`/api/account/${account.email}`, account);
  }

  getAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(`/api/account/`);
  }
}
