import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Account } from 'src/entities/account';
import { Preferences } from 'src/entities/preferences';
import { CredentialsService } from 'src/services/credentials/credentials.service';
import { PreferencesService } from 'src/services/preferences/preferences.service';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {

  currencies!: Currency[];
  selectedCurrency!: Currency;
  account!: Account;
  newPreferences!: boolean;

  constructor(private http: HttpClient,
    private preferencesService: PreferencesService,
    private credentialService: CredentialsService) {
    this.currencies = new Array<Currency>();
    this.http.get('assets/currency/supported_currencies.csv', { responseType: 'text' })
      .subscribe(
        data => {
          let csvToRowArray = data.split("\n");
          for (let index = 1; index < csvToRowArray.length - 1; index++) {
            let row = csvToRowArray[index].split("\t");
            this.currencies.push(new Currency(row[0], row[2].trim(), row[1]));
          }
          console.log(this.currencies);
        },
        error => {
          console.log(error);
        }
      );
  }

  ngOnInit(): void {
    this.credentialService.account$.subscribe(newAcc => {
      if (newAcc) {
        this.account = newAcc;
      }
    })
    this.loadPreferences();
  }

  async loadPreferences() {
    let preferences$ = this.preferencesService.getPreferences(this.account.email);
    let preferences = await lastValueFrom(preferences$);
    if (preferences != null) {
      for (let currency of this.currencies) {
        if (currency.id === preferences.currencyCode) {
          this.selectedCurrency = currency;
          this.newPreferences = false;
          return;
        }
      }
    }
    this.newPreferences = true;
  }

  async updatePreferences() {
    var preferences = new Preferences();
    preferences.currencyCode = this.selectedCurrency.id;
    preferences.user = this.account.email;
    if (this.newPreferences) {
      let createdPreferences$ = this.preferencesService.createPreferences(preferences);
      let createdPreferences = await lastValueFrom(createdPreferences$);
      if (createdPreferences) {
        window.location.reload();
      }
    }
    else {
      let createdPreferences$ = this.preferencesService.updatePreferences(preferences);
      let createdPreferences = await lastValueFrom(createdPreferences$);
      if (createdPreferences) {
        window.location.reload();
      }
    }
  }

}

class Currency {
  id!: string;
  country!: string;
  currencyName!: string;

  constructor(id: string, country: string, currencyName: string) {
    this.id = id;
    this.country = country;
    this.currencyName = currencyName;
  }
}
