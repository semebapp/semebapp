import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { AccountService } from 'src/services/account/account.service';
import { CredentialsService } from 'src/services/credentials/credentials.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  typedMail!: string;
  loginError: boolean = false;

  constructor(private accountService: AccountService,
    private credentialService: CredentialsService,
    private router: Router) { }

  ngOnInit(): void {
  }

  async login() {
    const account$ = this.accountService.getAccount(this.typedMail);
    const account = await lastValueFrom(account$);
    console.log(account)
    if (account !== null) {
      this.credentialService.setCredentials(account);
      this.router.navigate(['/overview'])
    }
    else {
      this.loginError = true;
    }
  }

  navigateToRegister(){
    this.router.navigate(['register']);
  }
}
