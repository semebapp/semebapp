import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Account } from 'src/entities/account';
import { CredentialsService } from 'src/services/credentials/credentials.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  display = false;
  items!: MenuItem[];
  account!: Account;

  constructor(
    private credentialSerivice: CredentialsService,
    private router: Router) { }

  ngOnInit(): void {
    this.credentialSerivice.account$.subscribe(nextAccount => {
      if (nextAccount === null) {
        this.display = false;
      }
      else {
        this.items = [{
          label: nextAccount.firstName + ' ' + nextAccount.lastName,
          items: [
            {
              label: 'Abmelden',
              icon: 'pi pi-refresh',
              command: () => {
                this.logout();
              }
            },
            {
              label: 'Einstellungen',
              icon: 'pi pi-refresh',
              command: () => {
                this.router.navigateByUrl('preferences');
              }
            },
          ]
        }]
        this.display = true;
        this.account= nextAccount;
      }
    });
  }

  logout() {
    this.credentialSerivice.removeCredentials();
  }

  getLabel(){
    return this.account.firstName.slice(0,1)+this.account.lastName.slice(0,1);
  }
}
