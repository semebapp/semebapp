import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Account } from 'src/entities/account';
import { AccountService } from 'src/services/account/account.service';
import { CredentialsService } from 'src/services/credentials/credentials.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm!: any;

  constructor(private accountService: AccountService,
    private router: Router,
    private credentialsService: CredentialsService) { }

  ngOnInit(): void {
    this.buildForms();
  }

  buildForms() {
    this.registerForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      iban: new FormControl('', Validators.required),
    });
  }

  async register() {
    let accToCreate = new Account();
    Object.assign(accToCreate, this.registerForm.value);
    accToCreate.amount = 0;
    var account$ = this.accountService.createAccount(accToCreate);
    var account = await lastValueFrom(account$);
    if (account !== null && account !== undefined) {
      this.credentialsService.setCredentials(account);
      this.router.navigateByUrl('overview');
    }
  }
}
