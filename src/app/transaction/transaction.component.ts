import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Account } from 'src/entities/account';
import { Transaction } from 'src/entities/transaction';
import { AccountService } from 'src/services/account/account.service';
import { CredentialsService } from 'src/services/credentials/credentials.service';
import { TransactionService } from 'src/services/transaction/transaction.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  transactionForm!: any;
  transaction!: Transaction;
  account!: Account;

  constructor(private accountService: AccountService,
    private router: Router,
    private credentialsService: CredentialsService,
    private transactionService: TransactionService) {
    this.credentialsService.account$.subscribe(newAcc => {
      if (newAcc) {
        this.account = newAcc;
      }
    })
  }

  ngOnInit(): void {
    this.buildForms();
  }

  buildForms() {
    this.transactionForm = new FormGroup({
      to: new FormControl('', [Validators.email, Validators.required]),
      fromAmount: new FormControl(0, Validators.required),
      description: new FormControl('', Validators.required),
    });
  }

  async transact() {
    this.transaction = new Transaction();
    Object.assign(this.transaction, this.transactionForm.value);
    this.transaction.from = this.account.email;
    this.transaction.transactionDate = new Date();
    let createdTransaction$ = this.transactionService.createTransaction(this.transaction);
    let createdTransaction = await lastValueFrom(createdTransaction$);
    if (createdTransaction && createdTransaction.toAmount) {
      this.router.navigateByUrl('overview');
    }
  }
}
