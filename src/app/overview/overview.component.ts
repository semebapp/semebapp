import { Component, OnInit } from '@angular/core';
import { from, lastValueFrom } from 'rxjs';
import { Account } from 'src/entities/account';
import { Transaction } from 'src/entities/transaction';
import { CredentialsService } from 'src/services/credentials/credentials.service';
import { TransactionService } from 'src/services/transaction/transaction.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  transactions!: Transaction[];
  account!: Account;

  constructor(private transactionService: TransactionService,
    private credentialsService: CredentialsService) {
    this.credentialsService.account$.subscribe(newAcc => {
      if (newAcc != null) {
        this.account = newAcc;
      }
    })
  }

  ngOnInit(): void {
    this.loadTransactions();
  }

  async loadTransactions() {
    const transactions$ = this.transactionService.getTransactionsForAccount(this.account);
    this.transactions = await lastValueFrom(transactions$);
    console.log(this.transactions)
  }

  getAmountForDisplay(transaction: Transaction) {
    if (transaction.from === this.account.email) {
      return transaction.fromAmount;
    }
    else {
      return transaction.toAmount;
    }
  }

  isPositive(transaction: Transaction) {
    if (transaction.from === this.account.email) {
      return false;
    }
    else {
      return true;
    }
  }

  getTransactionPartner(transaction: Transaction) {
    if (transaction.from === this.account.email) {
      return transaction.to;
    }
    else {
      return transaction.from;
    }
  }

  getAccountAmount() {
    let amount = 0;
    for (let transaction of this.transactions) {
      if (this.isPositive(transaction)) {
        amount += transaction.toAmount;
      }
      else {
        amount -= transaction.fromAmount;
      }
    }
    return amount;
  }

}
