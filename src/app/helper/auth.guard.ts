import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Account } from 'src/entities/account';
import { CredentialsService } from 'src/services/credentials/credentials.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  currentAccount!: Account | null;

  constructor(private credentialsService: CredentialsService,
    private router: Router) {
    this.credentialsService.account$.subscribe(newAcc => {
      this.currentAccount = this.currentAccount;
    })
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.currentAccount !== null) {
      console.log('allowed');
      return true;
    }
    console.log('deny');
    this.router.navigate(['/login']);
    return false;
  }

}
